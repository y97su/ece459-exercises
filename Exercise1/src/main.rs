// You should implement the following function:

fn sum_of_multiples(number: i32, multiple1: i32, multiple2: i32) -> i32 
{
    let mut sum = 0;
    for n in 1..number {
        if n % multiple1 == 0 {
            sum += n;
        }
        else if n % multiple2 == 0 {
            sum += n;
        }
    }
    return sum;
}

fn main() {
    println!("{}", sum_of_multiples(1000, 5, 3));
}