use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    let mut children = vec![];

    for i in 0..N {
        let thread_join_handle = thread::spawn(move || {
            println!("I am thread number {}", i);
        });
        children.push(thread_join_handle);
    }

    for ch in children {
        ch.join().expect("Expected thread to be joined");
    }
}
