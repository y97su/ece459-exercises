// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    if n == 0 {
        return 0;
    }

    let mut curr = 1;
    let mut prev1 = 1;
    let mut prev2 = 0;
    
    for _ in 1..n {
        curr = prev1 + prev2;
        prev2 = prev1;
        prev1 = curr;

    }
    return curr;
}


fn main() {
    println!("{}", fibonacci_number(10));
}
